open Locations

(* Mustache *)

let read_template file =
  let inchan = open_in file in
  let lexbuf = Lexing.from_channel inchan in
  let t = Mustache.parse_lx lexbuf in
  close_in inchan;
  t

let template ~src ~dst ~partials env =
  let t = read_template src in
  with_output dst (fun out ->
    Mustache.render_fmt ~strict:true ~partials out t env)

(* JSON *)

let read_json_ file =
  let inchan = open_in file in
  let json = Ezjsonm.from_channel inchan in
  close_in inchan;
  json

let read_json file =
  try read_json_ file with
  | e -> prerr_endline ("Error while reading " ^ file); raise e

let as_object v =
  match v with
  | `O obj -> obj
  | _ -> invalid_arg "This is not a JSON object"

let member : string -> (string * Ezjsonm.value) list -> Ezjsonm.value = List.assoc
let member_opt : string -> (string * Ezjsonm.value) list -> Ezjsonm.value option = List.assoc_opt

let hydrate_config config =
  let config = as_object config in
  let open Ezjsonm in
  let topic = config |> member "topic" |> get_string in
  let raw_topic = topic |> Soup.parse |> Soup.texts |> String.concat "" in
  (  ("raw-topic", string raw_topic)
  :: config
  )

(* Partials (mustache tags like {{> more-links}}) *)

let more_links config =
  let open Ezjsonm in
  let open Html in
  let item j =
    let obj = as_object j in
    a ~attrs:[href_ (obj |> member "url" |> get_string)]
      (obj |> member "name" |> get_string |> escape) in
  match member_opt "more-links" config with
  | None | Some (`A []) -> Html.empty
  | Some (`A js) ->
    let xs = List.map item js in
    concat [p (escape "More links:"); ul (List.map li xs)]
  | Some _ -> invalid_arg "more-links: this must be an array"
;;

let mk_partials config name : Mustache.t option =
  match name with
  | "more-links" -> Some (Mustache.raw (more_links config |> Html.to_string))
  | _ -> failwith ("Unknown partial: {{> " ^ name ^ "}}")
;;

(* Generate site *)

let current_time () =
  let now = Ptime_clock.now () in
  let b = Buffer.create 30 in
  let fmt = Format.formatter_of_buffer b in
  Ptime.pp fmt now;
  Format.pp_print_flush fmt ();
  Buffer.contents b

let mk_env ~body =
  [ ("body", `String body)
  ; ("update", `String (current_time ()))
  ]

let cpdir ?(ignore_missing_dir=false) src dst =
  let open FileUtil in
  if ignore_missing_dir && not (test Is_dir src) then ()
  else (
    mkdir ~parent:true dst;
    cp (ls src) dst
  )

let skip_if_doesnotexist msg err =
  match err with
  | `NoSourceFile _ -> ()
  | _ -> raise (FileUtil.CpError msg)
;;

let run_ locs env =
  let config = read_json locs.config_file |> hydrate_config in
  let partials = mk_partials config in
  let env = `O (env @ config) in
  FileUtil.mkdir ~parent:true locs.output_dir;
  template ~src:locs.index_from ~dst:locs.index_to ~partials env;
  template ~src:locs.style_from ~dst:locs.style_to ~partials env;
  cpdir ~ignore_missing_dir:true locs.assets_from locs.assets_to

let run ~topic ~body =
  run_ (locations ~topic) (mk_env ~body)
