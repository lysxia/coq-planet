(* Try to fix erroneous encodings.
   Hypothesis: UTF-8 was parsed as latin-1 or windows-1252 (observed in Planet Haskell), and then printed again as UTF-8.
   Solution: Detect if some sequences of Unicode code points < 256 (belonging to latin-1),
    coerced to sequences of bytes, are valid UTF-8.
    We add an extra pass to convert Unicode code points that belong to windows-1252 into bytes as well.
*)

exception BadUnicode
module Uchar = Stdlib.Uchar

let is_ascii (c : Uchar.t) : bool =
  c < Uchar.of_int 128

let lead (c : int) : int =
  if 0xC2 <= c && c <= 0xDF then 1
  else if 0xE0 <= c && c <= 0xEF then 2
  else if 0xF0 <= c && c <= 0xF4 then 3
  else 0

let is_follower (c : int) : bool =
  0x80 <= c && c <= 0xBF

let try_decode_uchar (s : string) : Uchar.t option =
  let decoder = Uutf.decoder ~encoding:`UTF_8 (`String s) in
  match Uutf.decode decoder with
  | `Uchar u -> Some u
  | `End -> None
  | `Malformed _ -> None
  | `Await -> assert false

let string_of_bytes (l : int list) =
  let l = ref l in
  String.init (List.length !l) (fun _ -> match !l with
    | [] -> assert false
    | u :: tl ->
      l := tl;
      Char.chr u)

let windows1252_hack (c : Uchar.t) : int =
  let c = Uchar.to_int c in
  if      c = 0x20AC then 0x80
  else if c = 0x201A then 0x82
  else if c = 0x0192 then 0x83
  else if c = 0x201E then 0x84
  else if c = 0x2026 then 0x85
  else if c = 0x2020 then 0x86
  else if c = 0x2021 then 0x87
  else if c = 0x02C6 then 0x88
  else if c = 0x2030 then 0x89
  else if c = 0x0160 then 0x8A
  else if c = 0x2039 then 0x8B
  else if c = 0x0152 then 0x8C
  else if c = 0x017D then 0x8E
  else if c = 0x2018 then 0x91
  else if c = 0x2019 then 0x92
  else if c = 0x201C then 0x93
  else if c = 0x201D then 0x94
  else if c = 0x2022 then 0x95
  else if c = 0x2013 then 0x96
  else if c = 0x2014 then 0x97
  else if c = 0x02DC then 0x98
  else if c = 0x2122 then 0x99
  else if c = 0x0161 then 0x9A
  else if c = 0x203A then 0x9B
  else if c = 0x0153 then 0x9C
  else if c = 0x017E then 0x9E
  else if c = 0x0178 then 0x9F
  else c

let fix_unicode_ (s : string) : string =
  let decoder = Uutf.decoder ~encoding:`UTF_8 (`String s) in
  let dst = Buffer.create (String.length s) in
  let encoder = Uutf.encoder `UTF_8 (`Buffer dst) in
  let dump = List.iter @@ fun u -> ignore (Uutf.encode encoder (`Uchar u))
  in
  let try_decode buffer hacked_buffer = match try_decode_uchar (string_of_bytes hacked_buffer) with
    | Some u -> ignore (Uutf.encode encoder (`Uchar u))
    | None
    | exception _ -> dump buffer
  in
  let rec go expected buffer hacked_buffer =
    match Uutf.decode decoder with
    | `Uchar c ->
      let u = windows1252_hack c in
      (* Printf.eprintf "Got %d/%d %s\n" (Uchar.to_int c) (Uchar.to_int u) (if is_follower u then "follower" else if lead u > 0 then "lead" else "noop"); *)
      if expected = 0 then
        let lead = lead u in
        if lead = 0 then
          (ignore (Uutf.encode encoder (`Uchar c)); go 0 [] [])
        else
          go lead [c] [u]
      else
        if is_follower u then
          if expected = 1 then
            (try_decode (List.rev (c :: buffer)) (List.rev (u :: hacked_buffer)); go 0 [] [])
          else
            go (expected - 1) (c :: buffer) (u :: hacked_buffer)
        else
          (dump (List.rev buffer); ignore (Uutf.encode encoder (`Uchar c)); go 0 [] [])
    | `End -> ignore (Uutf.encode encoder `End)
    | `Malformed _ -> raise BadUnicode
    | `Await -> assert false
  in go 0 [] [];
  Buffer.contents dst

let fix_unicode (s : string) : string =
  try fix_unicode_ s with
  | BadUnicode -> s

let%expect_test _ =
  print_endline (fix_unicode "whatâ€™s");
  [%expect {| what’s |}]
