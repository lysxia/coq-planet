set -xe
MYSERVER=kite
EXENAME=pl_anet.exe
EXE=_build/default/exe/$EXENAME
dune build exe/$EXENAME
dune exec exe/$EXENAME || true
chmod u+w $EXE
rsync -avK template config output $EXE $MYSERVER:coq-planet/
