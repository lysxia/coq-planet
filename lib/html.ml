(* Lightweight HTML library *)

type html = Buffer.t -> unit

type t = html

let empty : html = fun _ -> ()

let to_string (t : html) : string =
  let b = Buffer.create 1000 in
  t b;
  Buffer.contents b

let escape (s : string) : html =
  fun b -> Buffer.add_string b (Mustache.escape_html s)

let unsafe_raw (s : string) : html =
  fun b -> Buffer.add_string b s

type attr_value =
  [ `Str of string
  ]

type attr = string * attr_value

let class_ (cls : string) : attr = ("class", `Str cls)
let title_ (ttl : string) : attr = ("title", `Str ttl)
let href_ (r : string) : attr = ("href", `Str r)
let id_ (i : string) : attr = ("id", `Str i)

let tag ?(attrs : attr list = []) (name : string) (t : html) : html =
  fun b ->
    let (<<<) b s = Buffer.add_string b s; b in
    let (<<.) = Buffer.add_string in
    b <<< "<" <<. name;
    attrs |> List.iter (fun (key, value) ->
      b <<< " " <<. key;
      match value with
      | `Str value ->
        b <<. "='";
        escape value b;
        b <<. "'"
      );
    b <<. ">";
    t b;
    b <<< "</" <<< name <<. ">"

let concat (t : html list) : html =
  fun b ->
    List.iter (fun f -> f b) t

let a ?attrs = tag ?attrs "a"

let div ?attrs = tag ?attrs "div"

let p ?attrs = tag ?attrs "p"
let span ?attrs = tag ?attrs "span"

let h2 ?attrs = tag ?attrs "h2"

let ul ?attrs (xs : html list) = tag ?attrs "ul" (concat xs)
let li ?attrs = tag ?attrs "li"
