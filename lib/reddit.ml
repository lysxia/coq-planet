open Lwt
open Cohttp
open Common

(* Reddit API doc: https://www.reddit.com/dev/api *)
let sub_url ~subreddit = "https://www.reddit.com/r/" ^ subreddit
let auth_url ~subreddit = "https://oauth.reddit.com/r/" ^ subreddit
let api_url ~subreddit = auth_url ~subreddit ^ "/new?limit=10"

let feed_source ~subreddit =
  { feed_url = sub_url ~subreddit ^ ".rss"
  ; feed_title = "/r/" ^ subreddit
  }

type meta =
  { comments : int
  ; comments_url : string
  }

let render_meta m =
  m.comments
    |> show_count "comment"
    |> Option.map @@ fun t ->
         Html.a ~attrs:[Html.href_ m.comments_url] (Html.escape t)

(** Crossposts have relative URLs *)
let clean_url url =
  let starts_with_r =
    try url.[0] = '/' && url.[1] = 'r' && url.[2] = '/'
    with Invalid_argument _ -> false
  in
  if starts_with_r then
    "https://www.reddit.com" ^ url
  else
    url

let to_post j : html_entry =
  let open Yojson.Basic.Util in
  let j = j |> member "data" in
  { title = j |> member "title" |> to_string |> Title.unsafe_raw
  ; time = j |> member "created" |> to_float |> Time.time_of_unix_float
  ; url = j |> member "url" |> to_string |> clean_url
  ; author = j |> member "author" |> to_string |> (fun s -> "/u/" ^ s)
  ; meta = render_meta
      { comments = j |> member "num_comments" |> to_int
      ; comments_url = j |> member "permalink" |> to_string |> ((^) "https://www.reddit.com")
      }
  }

(** Reddit oauth *)
(* Get an authentication token to make requests with *)
(* References:
  - Doc https://github.com/reddit-archive/reddit/wiki/OAuth2#application-only-oauth
  - https://www.ranjithnair.dev/reddit-application-oauth/
  *)

let auth_url = "https://oauth.reddit.com/api/v1/access_token"

let headers = lazy
  (match Secrets.get_auth_ "reddit" with
  | `Basic (client_id, key) -> Header.of_list
    [ "Authorization", "Basic " ^ Base64.encode_string (client_id ^ ":" ^ key) ]
  | _ -> failwith "no reddit auth")

let fetch_token () =
  let (let*) = (>>=) in
  let* (resp, body) = Client.post
    ~headers:(Lazy.force headers)
    ~body:(`String "grant_type=client_credentials")
    (Uri.of_string auth_url) in
  let code = resp |> Response.status |> Code.code_of_status in
  body |> Cohttp_lwt.Body.to_string >|= fun body ->
    print_endline ("POST-ed " ^ auth_url);
    debug Printf.(fun h ->
      fprintf h "%s\n" auth_url;
      fprintf h "Response code: %d\n" code;
      fprintf h "Headers: %s\n" (resp |> Response.headers |> Header.to_string);
      fprintf h "Body: %s\n" body);
    match Response.status resp with
    | `OK ->
      let open Yojson.Basic.Util in
      let body = Yojson.Basic.from_string body in
      body |> member "access_token" |> to_string
    | _ -> failwith "Error: POST failed"

let lwt_token = lazy (fetch_token ())

(**)

let fetch ~url () : fetch_result =
  Lazy.force lwt_token >>= fun token ->
  cached_get ~headers:(Header.of_list ["Authorization", "Bearer " ^ token]) url >|= fun body ->
  let json = Yojson.Basic.from_string body in
  let open Yojson.Basic.Util in
  json
    |> member "data"
    |> member "children"
    |> to_list
    |> take 10
    |> List.map to_post

let source ~subreddit : source =
  { name = "Reddit"
  ; info = None
  ; url = sub_url ~subreddit
  ; unique_id = "reddit-" ^ subreddit
  ; feed_source = Some (feed_source ~subreddit)
  ; fetch = fetch ~url:(api_url ~subreddit)
  }
