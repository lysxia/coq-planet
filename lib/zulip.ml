open Lwt
open Cohttp
open Common

let is_basicchar c =
     c = '-'
  || ('a' <= c && c <= 'z')
  || ('A' <= c && c <= 'Z')
  || ('0' <= c && c <= '9')

let hex_char i =
  if i < 0 || 16 <= i then invalid_arg "Expected hex digit"
  else if i <= 9 then Char.chr (i + Char.code '0')
  else Char.chr (i - 10 + Char.code 'A')

let discourse_slug title =
  let b = Buffer.create 100 in
  let escape_char c =
    if is_basicchar c then Buffer.add_char b c
    else
      let (x, y) = (Char.code c / 16, Char.code c mod 16) in
      Buffer.add_char b '.';
      Buffer.add_char b (hex_char x);
      Buffer.add_char b (hex_char y)
  in
  String.iter escape_char title;
  Buffer.contents b

let topic_url ~user_base_url title = user_base_url ^ discourse_slug title

let mk_entry ~user_base_url title =
  { title = Title.escape title
  ; url = topic_url ~user_base_url title
  ; time = Time.invalid_time
  ; author = author_na
  ; meta = None
  }

let fetch ~api_url ~auth ~ignores ~user_base_url () : fetch_result =
  let headers = Header.add_authorization (Header.init ()) auth in
  cached_get ~headers api_url >|= fun body ->
  let json = Yojson.Basic.from_string body in
  let open Yojson.Basic.Util in
  json
    |> member "topics"
    |> to_list
    |> List.map (fun j -> j |> member "name" |> to_string)
    |> List.filter (fun title -> not (List.mem title ignores))
    |> Common.take 10
    |> List.map (mk_entry ~user_base_url)

let source_ ~topic ~url ~api_url ~auth ~user_base_url ~ignores () : source =
  basic_source ~topic ~url
    ~name:"Zulip"
    ~fetch:(fetch ~api_url ~auth ~ignores ~user_base_url)
    ()

let source ~topic ~url ~api_url ~auth ~user_base_url ~ignores () : source =
  match auth with
  | None -> empty_source ~name:"Zulip" ~info:(`InfoBg "requires login") ~url ~unique_id:(topic ^ "-zulip")
  | Some auth -> source_ ~topic ~url ~api_url ~auth ~user_base_url ~ignores ()
