open Lwt
open Common
open Time

let mk_url ~url (Year year) month ext =
  url ^ "/" ^ string_of_int year ^ "-" ^ name_of_month month ^ ext

let print_entry e =
  Common.print_entry (fun meta -> string_of_int meta.response_count) e

let render_meta (meta : 'a agg_meta) : Html.t option =
  meta.response_count
    |> show_count "reply" ~plural:"replies"
    |> Option.map Html.escape

let read_time (d : string) : time option =
  try
  Scanf.sscanf d "%_4s %2d %3s %4d %2d:%2d:%_2d"
    @@ fun cal_day mm cal_year tod_hour tod_minute -> Some
      { time_day =
          { cal_day
          ; cal_month = month_of_name_ mm
          ; cal_year
          }
      ; time_clock = Some { tod_hour ; tod_minute }
      }
  with
  | Scanf.Scan_failure _ -> None
;;

(* There are newlines and tabs *)
let rex_spaces = Re.Pcre.regexp "\\s+"
let clean_whitespace s =
  s |> String.trim
    |> Re.Pcre.substitute ~rex:rex_spaces ~subst:(fun _ -> " ")

let subject_rex_tag = Re.Pcre.regexp "\\[Haskell-cafe\\] |Fwd: "
let clean_subject_tag s =
  Re.Pcre.substitute ~rex:subject_rex_tag ~subst:(fun _ -> "") s

let clean_subject s = s |> clean_subject_tag |> clean_whitespace

let rex_headers = Re.Pcre.regexp ~flags:[`MULTILINE]
  "^Date:[ \t]*([^\n]*)\nSubject:[ \t]*(?:[^\n]*(?:\n[ \t]+[^\n]*)*)"

let process_txt body : time list =
  let open Re.Pcre in
  let rec matches ~pos acc =
    match exec ~pos ~rex:rex_headers body with
    | g ->
      let open Re.Group in
      let acc =
            match read_time (get g 1) with
            | Some e -> e :: acc
            | None -> acc
      in matches ~pos:(stop g 0) acc
    | exception Not_found -> acc
  in matches ~pos:0 []
;;

type light_entry = { title : Title.t ; url : string ; author : string }

let from_anchor ~url year month t =
  let open Soup in
  let title_a = t |> R.tag "a" in
  let title = title_a |> R.leaf_text |> clean_subject |> Title.escape in
  let url = mk_url ~url year month ("/" ^ R.attribute "href" title_a) in
  let author = t |> R.tag "i" |> R.leaf_text |> String.trim in
  { title ; url ; author }

let scrape_html ~url year month body : light_entry list =
  let open Soup in
  body |> parse $$ "ul"
    |> R.nth 2
    |> children
    |> elements
    |> to_list
    |> List.rev
    |> List.map (from_anchor ~url year month)

let rec zip_with f xs ys =
  match xs, ys with
  | x :: xs, y :: ys ->
    let z = f x y in z :: zip_with f xs ys
  | _, _ -> []
;;

let fetch_ ~url year month =
  let get ext = cached_get (mk_url ~url year month ext) in
  let get_dates = get ".txt" >|= process_txt in
  let get_urls = get "/date.html" >|= scrape_html ~url year month in
  get_dates >>= fun dates ->
  get_urls >>= fun urls ->
  let combine time { title ; url ; author } =
    { title ; url ; time ; author ; meta = () } in
  flush stdout;
  Lwt.return (zip_with combine dates urls)

let fetch ~url () : html_entry list Lwt.t =
  let (year, month) = current_ym () in
  let get_msg_curr_month = fetch_ ~url year month in
  let get_msg_prev_month =
    let (year, month) = previous_ym year month in
    fetch_ ~url year month in
  get_msg_curr_month >>= fun msg_curr_month ->
  get_msg_prev_month >|= fun msg_prev_month ->
  let msgs = msg_curr_month @ msg_prev_month in
  msgs
    |> Common.aggregate_entries
    |> Common.prerender render_meta

let source ~name ?info ~url ~unique_id () : source =
  { name ; info ; url ; unique_id
  ; feed_source = None
  ; fetch = fetch ~url
  }
