let secrets_file = "config/secrets.json"

let secrets_ () =
  let f = open_in secrets_file in
  let s = really_input_string f (in_channel_length f) in
  close_in f;
  s |> Yojson.Basic.from_string

let secrets = lazy (
  if FileUtil.test FileUtil.Is_file secrets_file then
    secrets_ ()
  else
    `Assoc [])

let warn_once =
  let r = ref (fun () -> ()) in
  r := (fun () ->
    Printf.fprintf stderr
      "WARNING: Ignoring authentication... (no '%s' file)\n%!" secrets_file;
    r := (fun () -> ()));
  fun () -> !r ()

let get_auth_ service =
  let open Yojson.Basic.Util in
  let j = Lazy.force secrets |> member service in
  let user = j |> member "user" |> to_string in
  let key = j |> member "key" |> to_string in
  if key = "" then failwith "Empty key: remember to write your credentials to secrets.json";
  `Basic (user, key)

let get_auth service =
  try Some (get_auth_ service) with
  | _ -> warn_once (); None
