(* Scraping https://funprog.srid.ca/haskell/ *)
(* https://github.com/srid/zulip-archive *)

open Lwt
open Common

let option_subst f x = Option.bind x f

let parse_time s =
  let open Time in
  Scanf.sscanf s "%4d-%2d-%2d %2d:%2d:%_2d"
    @@ fun cal_year cal_month cal_day tod_hour tod_minute ->
         { time_day = { cal_year ; cal_month ; cal_day }
         ; time_clock = Some { tod_hour ; tod_minute }
         }

let parse_messages s =
  Scanf.sscanf s "%d" (fun n -> n)

(**)

type meta = { messages : int }

let render_meta meta : Html.t option =
  meta.messages
    |> show_count "message" ~plural:"messages"
    |> Option.map Html.escape

(**)

let to_entry ~base_url i =
  let open Soup in
  let title_elt = i $ ".header" in
  Option.bind (title_elt |> leaf_text |> Option.map Title.escape) @@ fun title ->
  Option.bind (title_elt |> attribute "href") @@ fun url ->
  let url = base_url ^ url in
  Option.bind (i
    |> child_element
    |> option_subst leaf_text
    |> Option.map parse_time) @@ fun time ->
  Option.bind (i $ ".description"
    |> leaf_text
    |> Option.map parse_messages) @@ fun messages ->
  Some { title ; url ; time ; author = author_na ; meta = render_meta { messages } }

let fetch ?opts ~base_url ~url () : fetch_result =
  cached_get ?opts url >|= fun body ->
  let open Soup in
  parse body $$ ".item"
    |> to_list
    |> List.filter_map (to_entry ~base_url)
    |> take 10

(**)

let source ~opts ~topic ~base_url ~url : source =
  basic_source ~topic ~url:base_url
    ~name:"Zulip Archive"
    ~fetch:(fetch ~opts ~base_url ~url)
    ()
