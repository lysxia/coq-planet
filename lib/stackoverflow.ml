open Lwt
open Common

(* Common API all of Stack Exchange. We fetch both Stack Overflow and CS Theory Stack Exchange. *)

type sort = [ `Activity | `Creation ]

let string_of_sort (s : sort) =
  match s with
  | `Activity -> "activity"
  | `Creation -> "creation"

let pp_sort (s : sort) =
  match s with
  | `Activity -> "Activity"
  | `Creation -> "Creation"

let site_name = function
  | "cstheory" -> "Theoretical Computer Science Stack Exchange"
  | "stackoverflow" -> "Stack Overflow"
  | "codereview" -> "Code Review Stack Exchange"
  | _ -> "Stack Exchange"

(* StackExchange API doc: https://api.stackexchange.com/docs/questions *)
let mk_url ~tag ~sort ~site =
  "https://api.stackexchange.com/2.2/questions?pagesize=15&order=desc&sort=" ^ string_of_sort sort ^ "&tagged=" ^ tag ^ "&site=" ^ site

let feed_source ~tag ~sort ~site =
  let domain =
        if site = "stackoverflow" then "stackoverflow.com"
        else site ^ ".stackexchange.com" in
  { feed_url =
      "https://" ^ domain ^ "/feeds/tags?tagnames=" ^ tag ^ "&sort=" ^ string_of_sort sort
  ; feed_title =
      pp_sort sort ^ " questions tagged " ^ tag ^ " - " ^ site_name site
  }


(* +dependent-type+proof-assistants *)

type meta =
  { answers : int
  ; activity : Time.time
  ; creation : Time.time
  }

type question = meta Common.entry

let print_question q =
  let print_meta meta =
    let s = string_of_int meta.answers in
    s ^ " answers" in
  print_entry print_meta q

let render_meta m : Html.t option =
  m.answers
    |> show_count "answer"
    |> Option.map Html.escape

(**)

let fetch ~sort ~url () =
  cached_get url >|= fun body ->
  let json = Yojson.Basic.from_string body in
  let open Yojson.Basic.Util in
  let not_closed j =
    match j |> member "closed_date" with
    | `Null -> true
    | _ -> false in
  let to_question j =
    let meta =
      { answers = j |> member "answer_count" |> to_int
      ; activity = j |> member "last_activity_date" |> to_int |> Time.time_of_unix
      ; creation = j |> member "creation_date" |> to_int |> Time.time_of_unix
      } in
    let time =
      match sort with
      | `Activity -> meta.activity
      | `Creation -> meta.creation in
    let author = j |> member "owner" |> member "display_name" |> to_string in
    (* StackOverflow's API already gives escaped titles *)
    { title = j |> member "title" |> to_string |> Title.unsafe_raw
    ; url = j |> member "link" |> to_string
    ; time
    ; author
    ; meta = render_meta meta
    } in
  json
    |> member "items"
    |> to_list
    |> List.filter not_closed
    |> Common.take 10
    |> List.map to_question

(**)

module SO = struct

let source ~tag ~sort =
  let site = "stackoverflow" in
  { name = "Stack Overflow"
  ; info = None
  ; url = "https://stackoverflow.com/questions/tagged/" ^ tag
  ; unique_id = tag ^ "-" ^ site
  ; feed_source = Some (feed_source ~tag ~sort ~site)
  ; fetch = fetch ~sort ~url:(mk_url ~tag ~sort ~site)
  }

end

module TCS = struct

let source ~tag ~sort =
  let site = "cstheory" in
  { name = "CS Theory Stack Exchange"
  ; info = None
  ; url = "https://cstheory.stackexchange.com/questions/tagged/" ^ tag
  ; unique_id = tag ^ "-" ^ site
  ; feed_source = Some (feed_source ~tag ~sort ~site)
  ; fetch = fetch ~sort ~url:(mk_url ~tag ~sort ~site:"cstheory")
  }

end

module CR = struct

let source ~tag ~sort =
  let site = "codereview" in
  { name = "Code Review Stack Exchange"
  ; info = None
  ; url = "https://codereview.stackexchange.com/questions/tagged/" ^ tag
  ; unique_id = tag ^ "-" ^ site
  ; feed_source = Some (feed_source ~tag ~sort ~site)
  ; fetch = fetch ~sort ~url:(mk_url ~tag ~sort ~site)
  }

end

module PA = struct

let source ~tag ~sort =
  let site = "proofassistants" in
  { name = "Proof Assistants Stack Exchange"
  ; info = None
  ; url = "https://proofassistants.stackexchange.com/questions/tagged/" ^ tag
  ; unique_id = tag ^ "-" ^ site
  ; feed_source = Some (feed_source ~tag ~sort ~site)
  ; fetch = fetch ~sort ~url:(mk_url ~tag ~sort ~site:"proofassistants")
  }

end
