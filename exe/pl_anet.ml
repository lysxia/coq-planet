open Pl_anet

let itemize items = Lwt_main.run (Common.fetch_and_render_all items)

let mk_site
  ~(topic : string)
  ~(url : string)
  ~(name : string)
  (items : Common.source list)
  : unit =
  if Opt.topic_enabled topic then
    let output = itemize items in
    Template.run ~topic ~body:(Html.to_string output.html);
    Export_atom.run ~topic ~url ~name output.entries

module CoqZulip = struct

let url_get_stream_id = "https://coq.zulipchat.com/api/v1/get_stream_id?stream=Coq+users"
let coq_users_id = "237977"

let url_topics i = "https://coq.zulipchat.com/api/v1/users/me/" ^ i ^ "/topics"
let api_url = url_topics coq_users_id

let user_base_url = "https://coq.zulipchat.com/#narrow/stream/237977/topic/"

let source () : Common.source =
  let zulip_auth = Secrets.get_auth "zulip" in
  Zulip.source
    ~topic:"coq"
    ~url:"https://coq.zulipchat.com"
    ~api_url
    ~auth:zulip_auth
    ~user_base_url
    ~ignores:
      [ "New Stack Exchange question"
      ; "New Discourse topics" ]
    ()

end

let coq () : unit =
  let zulip = CoqZulip.source () in
  let coq_blog = Rss.source
        ~name:"Coq News"
        ~info:(`InfoBg "release announcements")
        ~url:"https://coq.inria.fr/news"
        ~feed_url:"https://coq.inria.fr/rss.xml" () in
  mk_site ~topic:"coq" ~url:"https://coq.pl-a.net" ~name:"Planet Coq"
    [ Discourse.source ~title:"Coq Discourse" ~url:"https://coq.discourse.group"
    ; Coqclub.source
    ; zulip
    ; Stackoverflow.PA.source ~tag:"coq" ~sort:`Activity
    ; Stackoverflow.SO.source ~tag:"coq" ~sort:`Activity
    ; Stackoverflow.TCS.source ~tag:"coq" ~sort:`Activity
    ; Reddit.source ~subreddit:"Coq"
    ]

let haskell () : unit =
  let mail_url l = "https://mail.haskell.org/pipermail/" ^ l in
  let haskell_cafe = Mailman.source
        ~name:"The Haskell Café mailing list"
        ~url:(mail_url "haskell-cafe")
        ~unique_id:"haskell-cafe" () in
(*
  let libraries = Mailman.source
        ~name:"The Libraries mailing list"
        ~url:(mail_url "libraries")
        ~unique_id:"haskell-libraries" () in
*)
  let planet_haskell = Rss.source
        (* Some servers ignore encoding headers!? :( *)
        ~opts:`Uncompressed
        ~name:"Planet Haskell"
        ~info:(`InfoBg "planet.haskell.org")
        ~url:"https://planet.haskell.org"
        ~feed_url:"https://planet.haskell.org/rss20.xml" () in
  (* TODO: This almost works with cohttp. But I'm stuck, at runtime,
     at a TLS error (`KeyTooSmall). So instead I use curl. *)
  let haskell_news = Atom.source
        ~opts:`Curl
        ~name:"Haskell Weekly"
        ~url:"https://haskellweekly.news"
        ~feed_url:"https://haskellweekly.news/newsletter.atom" () in
  (* Make the titles a tiny bit more informative *)
  let haskell_news =
    let open Common in
    let with_date e = Title.unsafe_raw
      (Title.to_string e.title ^ " - " ^ Time.string_of_yyyymmdd ~sep:'-' e.time.time_day) in
    haskell_news |> Common.map_source (fun e -> { e with title = with_date e }) in
  mk_site ~topic:"haskell" ~url:"https://haskell.pl-a.net" ~name:"Haskell Planetarium"
    [ Discourse.source ~title:"Haskell Discourse" ~url:"https://discourse.haskell.org"
    ; Reddit.source ~subreddit:"haskell"
    ; planet_haskell
    ; haskell_cafe
    ; Stackoverflow.SO.source ~tag:"haskell" ~sort:`Creation
    ; Stackoverflow.CR.source ~tag:"haskell" ~sort:`Creation
    ; haskell_news
    ]

(*
(* A small template to add a new site about topic PL_NAME *)
(* Copy-paste this just above and complete all the TODOs. *)

let _TODO_PL_NAME () =
  let source1 = Reddit.source ~subreddit:"TODO_PL_SUBREDDIT" in
  let source2 = Atom.source
        ~name:"TODO PL NEWS SITE"
        ~url:"TODO https://PL.NEWS.SITE"
        ~feed_url"TODO https://PL.NEWS.SITE/FEEDLOCATION.atom" () in
  (* TODO: add more sources here *)
  mk_site ~topic:"_TODO_PL_NAME"
          (* TODO: that string should be the name of the newly created directory under [config/] *)
    [ source1
    ; source2
    (* TODO: list all sources here *)
    ]

(* TODO: call [_TODO_PL_NAME] at the end of [main] below *)
*)

let main () =
  Opt.parse_args ();
  coq ();
  haskell ();
  ()

let () =
  match main () with
  | () -> Debug.clean_up ()
  | exception e ->
    Debug.dump ();
    raise e
