open Lwt
open Common

let entry_of (i : Syndic.Atom.entry) =
  let open Syndic.Atom in
  let title =
    match i.title with
    | Text title -> Some (Title.escape title)
    | Html (_, _) | Xhtml (_, _) -> None in
  let author = (fst i.authors).name in
  let time = Time.of_ptime i.updated in
  let url =
    match i.links with
    | [] -> ""
    | { href ; _ } :: _ -> Uri.to_string href in
  Option.bind title @@ fun title ->
  Some { title ; url ; time ; author ; meta = None }

let entries_of ~feed =
  feed.Syndic.Atom.entries
    |> List.filter_map entry_of
    |> List.sort desc_time
    |> take 10

let fetch ?opts ~feed_url () : fetch_result =
  cached_get ?opts feed_url >|= fun body ->
  let xml = Xmlm.make_input (`String (0, body)) in
  let feed = Syndic.Atom.parse xml in
  entries_of ~feed
;;

let source ?opts ?info ~name ~url ~feed_url () : source =
  let fetch = fetch ?opts ~feed_url in
  let unique_id = slug name in
  let feed_source = Some { feed_url ; feed_title = name } in
  (* I assume the name of any RSS feed make it pretty identifiable. *)
  { name ; info ; url ; feed_source ; fetch ; unique_id }
