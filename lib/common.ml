open Lwt
open Cohttp
module Client = Cohttp_lwt_unix.Client

let debug = Debug.debug

let slug =
  let open Re.Pcre in
  let rex = regexp "[^a-zA-Z0-9]+" in
  fun s ->
    s |> substitute ~rex ~subst:(fun _ -> "-")
      |> String.map Char.lowercase_ascii

let read_file file =
  let h = open_in file in
  let s = really_input_string h (in_channel_length h) in
  close_in h;
  s

let write_file file s =
  let h = open_out file in
  output_string h s;
  close_out h

(* Caching *)

(* If a source goes down or causes some problem, we don't want to kill the
   whole program over it. Whenever we succeed we record the HTTP response body
   and the generated HTML in a cache, so in case of failure we just reuse the
   last result. *)

let write_cache ~cache_name s =
  FileUtil.mkdir ~parent:true (Filename.dirname cache_name);
  let compressed = Ezgzip.compress s in
  write_file cache_name compressed

let read_cache ~cache_name =
  let compressed = read_file cache_name in
  Result.get_ok (Ezgzip.decompress compressed)

let cached_
      ~(to_string : 'a -> string)
      ~(from_string : string -> 'a)
      ~(cache_name : string)
      ~(name : string)
      (get_t : unit -> 'a Lwt.t) : 'a Lwt.t =
  Lwt.try_bind get_t
    (fun t ->
      write_cache ~cache_name (to_string t);
      Lwt.return t)
    (fun e ->
      Debug.signal_problem ("while processing source " ^ name);
      Debug.debug (fun h ->
        let e = Printexc.to_string e in
        Printf.fprintf h "Exception: %s\n" e;
        Printf.fprintf h "Using cache %s\n" cache_name;
        output_char h '\n');
      try Lwt.return (from_string (read_cache ~cache_name)) with
      | _ ->
        Debug.debug (fun h -> Printf.fprintf h "Could not use cache %s\n" cache_name);
        raise e)

let cached
      ~(to_string : 'a -> string)
      ~(from_string : string -> 'a)
      ?(now = false)
      ~(name : string)
      (get_t : unit -> 'a Lwt.t) : 'a Lwt.t =
  let cache_name = "cache/" ^ slug name ^ ".gz" in
  if now then
    Lwt.return (from_string (read_cache ~cache_name))
  else
    cached_ ~to_string ~from_string ~cache_name ~name get_t

let cached_raw ?now = cached ~to_string:(fun s -> s) ~from_string:(fun s -> s) ?now
let cached_html ?now = cached ~to_string:Html.to_string ~from_string:Html.unsafe_raw ?now

(* HTTP Requests *)

let get ?headers url =
  print_endline ("fetching " ^ url);
  Client.get ?headers (Uri.of_string url) >>= fun (resp, body) ->
  let code = resp |> Response.status |> Code.code_of_status in
  body |> Cohttp_lwt.Body.to_string >|= fun body ->
    print_endline ("fetched " ^ url);
    debug Printf.(fun h ->
      fprintf h "%s\n" url;
      fprintf h "Response code: %d\n" code;
      fprintf h "Headers: %s\n" (resp |> Response.headers |> Header.to_string);
      fprintf h "Body of length: %d\n" (String.length body));
    (resp, body)

let get_body ?headers url =
  get ?headers url >>= fun (resp, body) ->
  match Response.status resp with
  | `OK -> Lwt.return body
  | _ -> failwith "Error: GET failed"

let get_gzip ?headers url =
  let headers = Header.add_opt headers "accept-encoding" "gzip" in
  get_body ~headers url >>= fun body ->
  match Ezgzip.decompress body with
  | Ok body -> Lwt.return body
  | Error _ -> failwith "Error: decompress failed"

let curl ?headers url : string Lwt.t =
  match headers with
  | None -> Lwt_process.pread ~stderr:`Dev_null ("curl", [|"-L"; url|])
  | Some _ -> invalid_arg "not implemented: passing headers to curl requests"
;;

type http_opt =
  [ `Default
  | `Uncompressed
  | `Curl
  ]

let get' ?(opts : http_opt =`Default) ?headers url =
  match opts with
  | `Default -> get_gzip ?headers url
  | `Uncompressed -> get_body ?headers url
  | `Curl -> curl ?headers url
;;

let cached_get ?(opts : http_opt = `Default) ?headers url =
  let now = Opt.offline () in
  cached_raw ~now ~name:url (fun () -> get' ~opts ?headers url)

(**)

(* Some sources give already HTML-escaped titles, so we can just keep them as-is.
   For all others, we must remember to escape them. *)
module Title : sig
  type t
  val escape : string -> t
  val unsafe_raw : string -> t
  val to_html : t -> Html.t
  val to_string : t -> string
end = struct
  type t = RawTitle of string (* Invariant: HTML-escaped string *)
  let escape s = RawTitle (Mustache.escape_html s)
  let unsafe_raw s = RawTitle s
  let to_html (RawTitle s) = Html.unsafe_raw s
  let to_string (RawTitle s) = s
end

type 'meta entry =
  { title : Title.t
  ; url : string
  ; time : Time.time
    (* Note: the time field is not always meaningful,
       and then its rendering should be disabled. (e.g., zulip.ml) *)
  ; author : string
  ; meta : 'meta
  }

let author_na = "N/A"

type html_entry = Html.t option entry

let desc_time e1 e2 = Time.compare e2.time e1.time

let print_entry (print_meta : 'meta -> string) (e : 'meta entry) =
  Printf.printf "%s (%s; %s) | %s\n"
    (Title.to_string e.title)
    (Time.string_of_yymmdd e.time.time_day)
    (print_meta e.meta)
    e.url

let render_time (t : Time.time) =
  let open Html in
  let dow = Time.day_of_the_week t.time_day in
  let ymd = Time.string_of_yyyymmdd t.time_day in
  let timestring = dow ^ " " ^ ymd in
  span ~attrs:[class_ "item-time"; title_ timestring] (escape timestring)

let unescape_html_re = Re.Pcre.regexp "&(?:amp|quot|apos|gt|lt|#(\\d+));"

let unescape_html s =
  let open Re.Pcre in
  let b = Buffer.create (String.length s) in
  let s = full_split ~rex:unescape_html_re s in
  let rec loop s =
    match s with
    | Text t :: s -> Buffer.add_string b t; loop s
    | Delim "&amp;" :: s -> Buffer.add_char b '&'; loop s
    | Delim "&quot;" :: s -> Buffer.add_char b '"'; loop s
    | Delim "&apos;" :: s -> Buffer.add_char b '\''; loop s
    | Delim "&gt;" :: s -> Buffer.add_char b '>'; loop s
    | Delim "&lt;" :: s -> Buffer.add_char b '<'; loop s
    | Delim _ (* unicode *) :: Group (_, n) :: s ->
      (try
        let c = Uchar.of_int (int_of_string n) in
        Buffer.add_utf_8_uchar b c
      with _ -> ());
      loop s
    | (Delim _ | Group _ | NoGroup) :: s -> loop s
    | [] -> ()
  in loop s; Buffer.contents b

let title_div ~title = Html.div ~attrs:[Html.class_ "item-title item-part"; Html.title_ (unescape_html title)]
let meta_div = Html.div ~attrs:[Html.class_ "item-part item-part-meta item-before-sep"]

let render_entry (e : html_entry) : Html.t =
  let open Html in
  let title = Title.to_html e.title in
  let title = title_div ~title:(Title.to_string e.title) (a ~attrs:[("href", `Str e.url)] title) in
  let meta =
        match e.meta with
        | None -> empty
        | Some meta -> meta_div meta in
  let time =
        if Time.is_valid e.time
        then meta_div (render_time e.time)
        else Html.empty in
  concat [title; meta; time]

let prerender_entry (render_meta : 'meta -> Html.t option) (e : 'meta entry) : html_entry =
  { e with meta = render_meta e.meta }

let prerender (render_meta : 'meta -> Html.t option) : 'meta entry list -> html_entry list =
  List.map (prerender_entry render_meta)

(**)

type fetch_result = html_entry list Lwt.t

type feed_source =
  { feed_url : string
  ; feed_title : string
  }

type info
  = [ `InfoBg of string | `InfoFg of string * string (* subtitle, url *) ]

type source =
  { name : string
  ; info : info option
  ; url : string
  ; unique_id : string
    (* Uniquely identify both the source and the topic,
       to serve as the name of the cache. *)
  ; feed_source : feed_source option
  ; fetch : unit -> html_entry list Lwt.t
  }

let basic_source
      ~(topic : string)
      ~(name : string)
      ?(info : info option)
      ?(feed_source : feed_source option)
      ~(url : string)
      ~(fetch : unit -> 'a list Lwt.t)
      ()
  : source =
  { name ; info ; url ; fetch
  ; unique_id = slug (topic ^ "-" ^ name)
  ; feed_source
  }

let basic_source'
      ~(topic : string)
      ~(name : string)
      ?(info : info option)
      ?(feed_source : feed_source option)
      ~(url : string)
      ~(fetch : unit -> html_entry list Lwt.t)
      ()
  : source =
  basic_source ~topic ~name ?info ?feed_source ~url ~fetch ()

let map_source (f : html_entry -> html_entry) (src : source) : source =
  let fetch () = Lwt.map (List.map f) (src.fetch ()) in
  { src with fetch }

let empty_source ~name ~info ~url ~unique_id : source =
  { name ; info = Some info ; url ; unique_id
  ; feed_source = None
  ; fetch = (fun () -> Lwt.return [])
  }

(**)

type 'a agg_meta =
  { mutable response_count : int
  ; orig_meta : 'a
  }

let aggregate_entries ?(max_entries=10) (es : 'a entry list) : 'a agg_meta entry list =
  let seen : (Title.t, 'a agg_meta entry) Hashtbl.t = Hashtbl.create max_entries in
  let eat e =
    match Hashtbl.find_opt seen e.title with
    | None when Hashtbl.length seen < max_entries ->
      Hashtbl.add seen e.title {e with meta = { response_count = 0 ; orig_meta = e.meta } }
    | None -> ()
    | Some e' -> e'.meta.response_count <- 1 + e'.meta.response_count
  in
  List.iter eat es;
  seen |> Hashtbl.to_seq_values |> List.of_seq |> List.sort desc_time

(**)

type feed_entry = feed_source option entry

type source_output =
  { html : Html.t
  ; entries : feed_entry list
  }

let render_entries_html (src : source) (es : html_entry list) : Html.t =
  let open Html in
  let h2_attrs = [id_ ("source-" ^ slug src.name)] in
  let h2_attrs =
        match src.info with
        | None | Some (`InfoFg (_, _)) -> h2_attrs
        | Some (`InfoBg info) -> title_ ("(" ^ info ^ ")") :: class_ "source-info" :: h2_attrs in
  let a_title = a ~attrs:[("href", `Str src.url)] (escape src.name) in
  let elt_title =
        match src.info with
        | None | Some (`InfoBg _) -> a_title
        | Some (`InfoFg (title2, url2)) ->
            let a_subtitle = a ~attrs:[("href", `Str url2)] (escape title2) in
            concat [a_title; unsafe_raw " ("; a_subtitle; unsafe_raw ")"] in
  concat
    [ h2 ~attrs:h2_attrs elt_title
    ; ul ~attrs:[class_ "link-table"]
        (es |> List.map
          (fun e -> li ~attrs:[class_ "item-line"] (render_entry e)))
    ]

let render_entries (src : source) (entries : 'a entry list) : source_output =
  { html = render_entries_html src entries
  ; entries = List.map (fun e -> { e with meta = src.feed_source }) entries
  }

let fetch_source_ (src : source) : source_output Lwt.t =
  src.fetch () >|= render_entries src

let fetch_source_cached (src : source) : source_output Lwt.t =
  let name = src.unique_id ^ ".html" in
  let to_string t = Html.to_string t.html in
  let from_string s = { html = Html.unsafe_raw s ; entries = [] } in
  cached ~name ~to_string ~from_string (fun () -> fetch_source_ src)

(* IF even the cache fails, render an empty source. *)
let fetch_source (src : source) : source_output Lwt.t =
  Lwt.catch (fun () -> fetch_source_cached src)
    (fun _ -> Lwt.return (render_entries src []))

(**)

let rec fetch_all_sources (srcs : source list) : source_output list Lwt.t =
  match srcs with
  | [] -> Lwt.return []
  | src :: srcs ->
    let x = fetch_source src in
    let ys = fetch_all_sources srcs in
    x >>= fun x ->
    ys >>= fun ys ->
    Lwt.return List.(x :: ys)
;;

let render_all_html (items : source_output list) : Html.t =
  let open Html in
  let mk_item i = div ~attrs:[class_ "grid-item"] i.html in
  div ~attrs:[class_ "grid-box"] (items |> List.map mk_item |> Html.concat)

let fetch_and_render_all (items : source list) : source_output Lwt.t =
  items |> fetch_all_sources >|= fun items ->
  { html = render_all_html items
  ; entries = List.concat_map (fun i -> i.entries) items
  }

(**)

let run f x = f x; x

let show_count singular ?plural n =
  if n = 0 then None
  else if n = 1 then Some ("1 " ^ singular)
  else
    let plural = match plural with
                 | None -> singular ^ "s"
                 | Some plural -> plural in
    Some (string_of_int n ^ " " ^ plural)

let rec take n xs =
  match xs with
  | _ when n = 0 -> []
  | x :: xs -> x :: take (n-1) xs
  | [] -> []
