set -xe
MYSERVER=kite
REMOTE_HOME=/home/sam
mkdir -p systemd/user
cat > systemd/user/planet.service << END
[Unit]
Description=Update pl-a.net

[Service]
ExecStart=$REMOTE_HOME/coq-planet/run.sh
StandardOutput=file:$REMOTE_HOME/coq-planet/log
TimeoutStartSec=60s
END
cat > systemd/user/planet.timer << END
[Unit]
Description=Timer to update pl-a.net

[Timer]
OnBootSec=60s
OnUnitActiveSec=20m
Unit=planet.service

[Install]
WantedBy=timers.target
END
cat > run.sh << 'END'
#!/bin/sh
set -e
cd coq-planet
./pl_anet.exe
END
chmod u+x run.sh
rsync -a systemd $MYSERVER:.config/
rsync -a run.sh $MYSERVER:coq-planet/
