open Lwt
open Common

type entry = unit Common.entry

let (<|>) x y =
  match x with
  | Some _ -> x
  | None -> y
;;

let entry_of_rss_item ?known_author i : html_entry option =
  let open Syndic.Rss2 in
  let title =
    match i.story with
    | All (title, _, _) | Title title -> Some (Title.escape (Fix_unicode.fix_unicode title))
    | Description (_, _) -> None in
  let time = Option.map Time.of_ptime i.pubDate in
  let url = Option.fold ~none:"" ~some:Uri.to_string i.link in
  Option.bind time @@ fun time ->
  (* Planet Haskell is bugged: entries in the future stay at the top even after the source removed it.
   *)
  if Time.(compare (current_time ()) time) < 0 then None else
  Option.bind title @@ fun title ->
  let author = Option.value ~default:author_na (known_author <|> i.author) in
  Some { title ; url ; time ; author ; meta = None }

let entries_of ~rss =
  rss.Syndic.Rss2.items
    |> List.filter_map entry_of_rss_item
    |> List.sort desc_time
    |> take 10

let fetch ?opts ~feed_url () : fetch_result =
  cached_get ?opts feed_url >|= fun body ->
  let xml = Xmlm.make_input (`String (0, body)) in
  let rss = Syndic.Rss2.parse xml in
  entries_of ~rss
;;

let source ?opts ?info ~name ~url ~feed_url () : source =
  let fetch = fetch ?opts ~feed_url in
  let unique_id = slug name in
  let feed_source = Some { feed_url = url ; feed_title = name } in
  (* I assume the name of any RSS feed make it pretty identifiable. *)
  { name ; info ; url ; feed_source ; fetch ; unique_id }
