type path = string

type locations =
  { index_from : path
  ; style_from : path
  ; config_dir : path
  ; config_file : path
  ; assets_from : path
  ; output_dir : path
  ; index_to : path
  ; assets_to : path
  ; style_to : path
  }

(* All file locations are defined upfront here. *)
let locations ~topic =
  let config_dir = "config/" ^ topic in
  let output_dir = "output/" ^ topic in
  { index_from = "template/index.html"
  ; style_from = "template/style.css"
  ; config_dir
  ; config_file = config_dir ^ "/config.json"
  ; assets_from = config_dir ^ "/assets"
  ; output_dir
  ; index_to = output_dir ^ "/index.html"
  ; assets_to = output_dir ^ "/assets"
  ; style_to = output_dir ^ "/style.css"
  }

let icon = "/assets/favicon-16x16.png"
let atom = "/atom.xml"

(* File output *)

let with_output file print_to =
  let out = open_out file in
  let fmt = Format.formatter_of_out_channel out in
  print_to fmt;
  Format.pp_print_flush fmt ();
  close_out out
