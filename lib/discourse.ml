open Lwt
open Common

(* Discourse API doc: https://docs.discourse.org/ *)
let topics_url ~url = url ^ "/latest.json?order=created"

let feed_source ~title ~url =
  { feed_url = url ^ "/latest.rss"
  ; feed_title = title
  }

(**)

type meta =
  { replies : int
  }

type entry = meta Common.entry

let print_entry e =
  let print_meta meta =
    string_of_int meta.replies ^ " replies" in
  Common.print_entry print_meta e

let render_meta (m : meta) : Html.t option =
  m.replies
    |> show_count "answer"
    |> Option.map Html.escape

(**)

let link_from_id ~url i = url ^ "/t/" ^ string_of_int i

let fetch ~url () : fetch_result =
  cached_get (topics_url ~url) >|= fun body ->
  let json = Yojson.Basic.from_string body in
  let open Yojson.Basic.Util in
  let to_topic j =
    { title = j |> member "title" |> to_string |> Title.escape
    ; url = j |> member "id" |> to_int |> link_from_id ~url
    ; time = j |> member "last_posted_at" |> to_string |> Time.parse_iso
    ; author = j |> member "last_poster_username" |> to_string
    ; meta = render_meta
        { replies = j |> member "posts_count" |> to_int |> (fun x -> x - 1)
        }
    } in
  let topics = json
    |> member "topic_list" |> member "topics" |> to_list
    |> take 10
    |> List.map to_topic
    |> List.sort desc_time in
  topics

let source ~title ~url : source =
  { name = "Discourse"
  ; info = None
  ; url
  ; unique_id = "discourse-" ^ string_of_int (Hashtbl.hash url)
  ; feed_source = Some (feed_source ~title ~url)
  ; fetch = fetch ~url
  }
