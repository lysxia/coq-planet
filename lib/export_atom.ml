
let mk_author name = Syndic.Atom.{
  name;
  uri = None;
  email = None;
}

let mk_source (s : Common.feed_source) : Syndic.Atom.source =
  Syndic.Atom.source
    ?categories:None ?contributors:None ?generator:None ?icon:None ?links:None
    ?logo:None ?rights:None ?subtitle:None ?updated:None
    ~authors:[]
    ~id:(Uri.of_string s.feed_url)
    ~title:(Text s.feed_title)

let mk_entry (e : Common.feed_entry) : Syndic.Atom.entry option =
  let open Syndic.Atom in
  Option.bind (Time.to_ptime e.time) @@ fun time ->
  let elink = link ~title:(Common.Title.to_string e.title) (Uri.of_string e.url) in
  Some (entry
    ?source:(Option.map mk_source e.meta)
    ~links:[elink]
    ~id:(Uri.of_string e.url)
    ~authors:(mk_author e.author, [])
    ~title:(Text (Common.Title.to_string e.title))
    ~updated:time
    ())

let feed ~url ~name entries =
  let open Syndic.Atom in
  let slink = link ~title:name ~rel:Self (Uri.of_string (url ^ Locations.atom)) in
  let alink = link ~title:name (Uri.of_string url) in
  feed
    ~id:(Uri.of_string url)
    ~title:(Text name)
    ~updated:(Time.current_ptime ())
    ~links:[slink; alink]
    ~icon:(Uri.of_string (url ^ Locations.icon))
    (List.filter_map mk_entry entries)

let run ~topic ~url ~name entries =
  let open Locations in
  let l = locations ~topic in
  let output = l.output_dir ^ Locations.atom in
  Syndic.Atom.write (feed ~url ~name entries) output

