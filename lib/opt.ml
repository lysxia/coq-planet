open Arg

module Internal = struct

let topic = ref None

let opts = ref
  [ "--topic", String (fun t -> topic := Some t), "TOPIC    Only generate the site for TOPIC"
  ]

let create key doc =
  let r = ref false in
  opts := (key, Set r, doc) :: !opts;
  fun () -> !r

end

open Internal

let skip_coqclub =
  create "--skip-coqclub" "  Don't fetch Coq Club (which is slow)"
let offline =
  create "--offline" "       Don't do HTTP requests (use cache if possible)"

let topic_enabled t =
  match !topic with
  | None -> true
  | Some t' -> t = t'
;;

let parse_args () =
  Arg.parse !opts (fun s -> raise (Bad s)) "pl-a.net link aggregator"
